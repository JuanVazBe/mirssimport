package com.cesur;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.tree.FixedHeightLayoutCache;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
/**
 * Hello world!
 *
 */
public class App 
{
    private static String serverDB="localhost";
    private static String portDB="1533";
    private static String DBname="ExamenRA4";
    private static String userDB="sa";
    private static String passwordDB="12345Ab##";
    private static Scanner sc=new Scanner(System.in);
    private static bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
    
    /** 
     * @param args
     */
    public static void main( String[] args ) {
   
   
    /*  ¡¡INSTRUCIONES DE USO DEL PROGRAMA!!
    *   Ejecutalo 1 vez contestando las prefuntas iniciales
    *   y ejecuta los CASE en ORDEN.
    */  

    
        CrearBBDD();
        //PROYECTO BBDD-DAW

        
	    boolean salirMenu= false;

        //Las preguntas las hago fuera porque me daba muchos problemas el hacerlas directamente en su case
        //tengo algun bug o algo que directamente ejecuta el print y se acaba la ejecucion del case :(.
        System.out.println("Introduce el rss :");
        String url=sc.nextLine();
        System.out.println("Elije una categoria para el CASE 5 :(1,2,3)");
        String op=sc.nextLine();
        System.out.println("Introduce la palabra para el CASE 4: ");
        String palabra=sc.nextLine();

        
        do {
	    	System.out.println("1. Importar RSS -> en el paso siguiente, deberemos introducir una URL");
	    	System.out.println("2. Ver ultimos 5 post");
	    	System.out.println("3. Ver todos los post");
	    	System.out.println("4. Buscar por titulo o contenido una palabra -> en el paso siguiente, indicaremos la palabra por la que buscar.");
	    	System.out.println("5. Filtrar por categoria -> en el paso siguiente mostraremos las categorias y luego escogeremos una y nos mostrará los 10 ultimos post de esa categoria.");
            System.out.println("6. Ver en detalle un solo post (Indicandole el ID).");
	    	int menu = sc.nextInt();
	    	
	    	switch (menu) {
	    		case 1:	   
                    XmlParse x =new XmlParse();
                    List<String>  titulos = x.leer("//item/title/text()",url);
                    List<String>  descripcion = x.leer("//item/description/text()",url);
                    List<String>  fecha = x.leer("//item/pubDate/text()",url);
                    List<String>  autores = x.leer("//item/creator/text()",url);
                    List<String>  categoria = x.leer("//item/creator/category()",url); //Cada vez que se ejecuta el programa sale un fallo al leer las categorias por ser Uncategorized.
                    System.out.println(("Total post: " + titulos.size()));

                    //INSERT DE CATEGORIAS

                    ArrayList <String> defecto= new ArrayList<>();
                    defecto.add("categoria1");
                    defecto.add("categoria2");
                    defecto.add("categoria3");

                    for(int i =0;i<3;i++) {
                        if(categoria.isEmpty()){
                            b.modificarBBDD("INSERT INTO Categorias(tipo) VALUES('"+defecto.get(i)+"')");
                         }else{
                            //b.modificarBBDD("INSERT INTO Categorias(tipo) VALUES('"+categoria.get(i)+"')");
                            //La line de arriba da fallo al ser las categorias 'Uncategorized' las interpreta como null o algo asi ya que no me las inserta
                            //en las BBDD.
                         }
                    }


                for (int i = 0; i < titulos.size(); ++i) {
                        String idfk="select ID_Usu from Usuarios where nombre='"+autores.get(i)+"'";
                        String idd=b.leerBBDDUnDato(idfk);
                        String idfkP="select ID_Post from Post";

                //INSERT DE USUARIOS

                        if (idd=="") {
                            b.modificarBBDD("INSERT INTO Usuarios(nombre) VALUES('"+autores.get(i)+"')");
                            idd=b.leerBBDDUnDato(idfk);
                        }


                //INSERT DE POST

                    String idfk3="select ID_Usu from Usuarios";
                    String idd3=b.leerBBDDUnDato(idfk3);

                    String idfk5="select ID_Cat from Categorias";
                    String idd5=b.leerBBDDUnDato(idfk3);

                    int num2 = (int)(Math.random()*3+1);

                    b.modificarBBDD("INSERT INTO Post(ID_FK_usu, ID_FK_cat, Titulo, Descripcion, Fecha) VALUES('"+idd3+"', '"+num2+"','"+titulos.get(i)+"','"+descripcion.get(i)+"','"+fecha.get(i)+"')");
                    idd3=b.leerBBDDUnDato(idfk3);

                }

            
                System.out.println("Datos introducidos en la base de datos correctamente.");

	    			break;
	    		case 2:
	    			b.leerBBDD("select top 5*from Post", 5);
	    			break;
	    		case 3:
	    			b.leerBBDD("SELECT * FROM Post", 6);
	    			break;
	    		case 4:
                    String titulo="select ID_Post, Titulo, Fecha from Post where Titulo like '%"+palabra+"%'";
                    System.out.println(b.leerBBDD(titulo, 3));
	    			break;
	    		case 5:
                    b.leerBBDD("SELECT ID_Post, ID_FK_Cat, Titulo FROM Post  where ID_FK_Cat='"+op+"'", 3);
	    			break;
                case 6:
                    System.out.println("Introduce el ID del post que quieres ver.");
                    int id=sc.nextInt();
                    String query="SELECT * FROM Post WHERE ID_Post='"+id+"'";
                    b.leerBBDD(query,5);
                    break;
	    	}
            
	    	
	    }while (!salirMenu);

	
        String kuery="Select * from clientes";
        LeerScriptBBDD(kuery);

    }



    private static void CrearBBDD() {
        bbdd b = new bbdd(serverDB,portDB,"master",userDB,passwordDB);
        
        String i= b.leerBBDDUnDato("SELECT count(1) FROM sys.databases  where name='"+DBname+"' ");
       // String cero = "0";
        if (i.equals("0")){
            b.modificarBBDD("CREATE DATABASE " + DBname);
            b=new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
            String query = LeerScriptBBDD("scripts/CrearBBDD.sql");
          
            b.modificarBBDD(query);
            b.modificarBBDD("insert into clientes values ('Angeles','angeles@cesur.com','666666666','calle falsa 13','41008','Sevilla')");
      
        }
    }

    private static String LeerScriptBBDD(String archivo){
       String ret = "";
        try {
            String cadena;
            FileReader f = new FileReader(archivo);
            BufferedReader b = new BufferedReader(f);
            while((cadena = b.readLine())!=null) {
                ret =ret +cadena;
            }
            b.close();
        } catch (Exception e) {
        }
       return ret;

    }
    
    public static void rellenarCat(ArrayList<String> categoria, bbdd b){
       
    }
}
