
DROP TABLE IF EXISTS  Clientes 

CREATE TABLE Clientes (
Id int IDENTITY(1,1) PRIMARY KEY,
Nombre Varchar(100) NOT NULL,
Email varchar(200) NOT NULL,
Telefono varchar(200) NOT NULL,
Direccion varchar(100) NOT NULL,
Cod_Postal varchar(100) NOT NULL,
Ciudad varchar(100) NOT NULL
)
