select * from Usuarios 
select * from Post 
select * from Categorias
select top(1) ID_Usu from Usuarios
 --Deb�is dise�ar una base de datos para un blog ( estilo worpress simplificado) 
 --donde tendremos varios usuarios que podr�n escribir varios post. 
-- Los post se pueden agrupar por categorias. Si no se asigna categoria a un post,
 --deberemos configurar un trigger para que se le asigne una por defecto, al igual que el usuario.

 -----------------------------CREATE TABLES-------------------------
create table Usuarios(
ID_Usu int identity(1,1) PRIMARY KEY,
Nombre nvarchar(50) not null,
)
drop table Usuarios
truncate table Usuarios

Create table Categorias(
ID_Cat int identity(1,1) not null PRIMARY KEY,
Tipo nvarchar(50) not null,
)
truncate table Categorias
drop table Categorias

declare @palabra as varchar(10)
select Titulo from Post where Titulo='%@palabra%'

create table Post(
ID_Post int identity(1,1) not null,
ID_FK_usu int not null,
ID_FK_cat int,
Titulo nvarchar(100) not null,
Descripcion nvarchar(3000) not null,
Fecha nvarchar(100) not null,
PRIMARY KEY (ID_Post),
CONSTRAINT ID_FK_usu FOREIGN KEY (ID_FK_usu) REFERENCES Usuarios(ID_Usu),
CONSTRAINT ID_FK_cat FOREIGN KEY (ID_FK_usu) REFERENCES Categorias(ID_Cat),
)
drop table Post
truncate table post

select max(ID_Cat) from Categorias 
--------------TRIGGER CATEGORIA-------------------
select * from post
drop trigger categoria
CREATE TRIGGER categoria
ON Post
FOR INSERT
AS BEGIN 
declare @idCat as int=(select ID_FK_Cat from inserted)
declare @idPost as int =(select ID_Post from inserted)
if @idCat is null
BEGIN
update Post set ID_FK_Cat = 1 where ID_Post = @idpost
END
END
		

-----------------------------INSERTS-------------------------
 select *from Usuarios
 select *from Post
 select *from Categorias
 SELECT ID_Post, ID_FK_Cat, Titulo, Fecha FROM Post  where Tipo='categoria2'

BEGIN TRAN
INSERT INTO Usuarios(ID_Usu, Nombre, Apellidos, Alias ,Correo)VALUES ('1','Juan', 'vazquez', 'JVB33', 'juanvazben@gmail.com')
INSERT INTO Usuarios(ID_Usu, Nombre, Apellidos, Alias ,Correo)VALUES ('2','Pepe', 'benitez', 'PBL22', 'pepebenlor@gmail.com')
INSERT INTO Usuarios(ID_Usu, Nombre, Apellidos, Alias ,Correo)VALUES ('3','Carmen', 'luque', 'CVB11', 'carmenvazben@gmail.com')
INSERT INTO Usuarios(ID_Usu, Nombre, Apellidos, Alias ,Correo)VALUES ('4','Loli', 'lora', 'LBL13', 'lolabenlor@gmail.com')
INSERT INTO Usuarios(ID_Usu, Nombre, Apellidos, Alias ,Correo)VALUES ('5','Jose', 'diaz', 'JBL21', 'josebenlor@gmail.com')
INSERT INTO Usuarios(ID_Usu, Nombre, Apellidos, Alias ,Correo)VALUES ('6','Pedro', 'rodriguez', 'PR54', 'pedrorodri@gmail.com')
INSERT INTO Usuarios(ID_Usu, Nombre, Apellidos, Alias ,Correo)VALUES ('7','Manuel', 'melero', 'MM89', 'manumele@gmail.com')
INSERT INTO Usuarios(ID_Usu, Nombre, Apellidos, Alias ,Correo)VALUES ('8','Carlos', 'arqui�ano', 'CA14', 'carlosgmail.com')
INSERT INTO Usuarios(ID_Usu, Nombre, Apellidos, Alias ,Correo)VALUES ('9','Pablo', 'berdugo', 'PB76', 'pablober@gmail.com')
INSERT INTO Usuarios(ID_Usu, Nombre, Apellidos, Alias ,Correo)VALUES ('10','Roberto', 'fofo', 'RF90', 'roberto@gmail.com')
ROLLBACK TRAN

BEGIN TRAN
INSERT INTO Post(ID_FK_usu,  ID_FK_cat, Titulo, Descripcion, Fecha)VALUES ('1','2','La taberna', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',getdate())
INSERT INTO Post(ID_Post, ID_FK_usu, Titulo, Descripcion ,Likes)VALUES ('2','5', 'Ricon', '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', '38')
INSERT INTO Post(ID_Post, ID_FK_usu, Titulo, Descripcion ,Likes)VALUES ('3','2', 'Yala', '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', '43')
INSERT INTO Post(ID_Post, ID_FK_usu, Titulo, Descripcion ,Likes)VALUES ('4','8', 'Eseranza abrumadora', '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', '54')
INSERT INTO Post(ID_Post, ID_FK_usu, Titulo, Descripcion ,Likes)VALUES ('5','3', 'Ley de Carl', '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', '42')
INSERT INTO Post(ID_Post, ID_FK_usu, Titulo, Descripcion ,Likes)VALUES ('6','9', 'Sin bollo', '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', '432')
INSERT INTO Post(ID_Post, ID_FK_usu, Titulo, Descripcion ,Likes)VALUES ('7','4', 'Comienzo', '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', '434')
INSERT INTO Post(ID_Post, ID_FK_usu, Titulo, Descripcion ,Likes)VALUES ('8','7', 'AKA', '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', '23')
INSERT INTO Post(ID_Post, ID_FK_usu, Titulo, Descripcion ,Likes)VALUES ('9','6', 'Trombosis', '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', '54')
INSERT INTO Post(ID_Post, ID_FK_usu, Titulo, Descripcion ,Likes)VALUES ('10','10', 'Vacuna y Agenda 2030', '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', '76')
ROLLBACK TRAN

--Categoria 1: Memes
--Categoria 2: Noticias
--Categoria 3: Publicidad

BEGIN TRAN
INSERT INTO Categorias(Tipo)VALUES ('')
INSERT INTO Categorias( Tipo, ID_FK_post)VALUES ('', '2')
INSERT INTO Categorias(ID_Cat, Tipo, ID_FK_post)VALUES ('3','Memes', '7')
INSERT INTO Categorias(ID_Cat, Tipo, ID_FK_post)VALUES ('4','Publicidad', '8')
INSERT INTO Categorias(ID_Cat, Tipo, ID_FK_post)VALUES ('5','Publicidad', '5')
INSERT INTO Categorias(ID_Cat, Tipo, ID_FK_post)VALUES ('6','Noticias', '1')
INSERT INTO Categorias(ID_Cat, Tipo, ID_FK_post)VALUES ('7','Memes', '3')
INSERT INTO Categorias(ID_Cat, Tipo, ID_FK_post)VALUES ('8','Publicidad', '9')
INSERT INTO Categorias(ID_Cat, Tipo, ID_FK_post)VALUES ('9','Memes', '4')
INSERT INTO Categorias(ID_Cat, Tipo, ID_FK_post)VALUES ('10','Noticias', '10')
ROLLBACK TRAN
use ExamenRA4
